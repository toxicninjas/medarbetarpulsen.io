﻿using Medarbetarpulsen.Website.App_Start;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Medarbetarpulsen.Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register); // <---------- THIS HAS TO BE FIRST?!?!?!?! https://stackoverflow.com/questions/28615741/api-2-controller-not-working-in-asp-net-mvc-5
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //this.BeginRequest += HandleRequest;

        }


        //private void HandleRequest(object source, EventArgs e)
        //{
        //    var context = ((HttpApplication)source).Context;
        //    var request = context.Request;

        //    var anus = "";

        //}
    }
}

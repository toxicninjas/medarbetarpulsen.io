﻿using Medarbetarpulsen.Core.Authentication;
using Medarbetarpulsen.Core.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Medarbetarpulsen.Website.Controllers.Developer
{
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {
        UserManager manager = new UserManager();

        [HttpGet]
        public IHttpActionResult Testing() 
        {
            return Ok("TEST TEST TEST");
        } 

        [Route("createidentity")]
        [HttpGet]
        public IHttpActionResult CreateUser(string email, string organizationName, string password)
        {
            var result = manager.CreateUser(email, organizationName, password);

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult DeleteIdentity(int id)
        {
            var result = manager.DeleteUser(id);

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult Login(string email, string password)
        {
            var result = manager.Login(email, password);

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult Test123()
        {
            var result = DbContext.GetLanguages();

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult CreateResetToken(string email)
        {
            var result = manager.CreateResetToken(email);

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult CheckResetToken(int token)
        {
            var result = manager.CheckResetToken(token);

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult SetPassword(int id, string password)
        {
            var result = manager.SetPassword(id, password);

            return Ok(result);
        }
    }
}

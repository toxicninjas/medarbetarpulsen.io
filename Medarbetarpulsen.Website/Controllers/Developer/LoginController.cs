﻿using Medarbetarpulsen.Core.Authentication;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Medarbetarpulsen.Website.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        UserManager manager = new UserManager();

        [EnableCors("*", "*", "*")]
        [Route("loginuser")]
        [HttpGet]
        public IHttpActionResult Login(string email, string password)
        {
            var result = manager.Login(email, password);

            if(result != null)
            {
                return Ok(result);
            }
            return BadRequest("ERROR WHEN TRYING TO LOGIN");
        }

        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult ResetPassword(string email, DateTime? expiresWhen = null)
        {
           var result =  manager.CreateResetToken(email, expiresWhen);
            if (!string.IsNullOrEmpty(result))
            {
                return Ok(result);
            }

            return BadRequest();
        }
    }
}

﻿using Medarbetarpulsen.Core.BusinessLogic;
using Medarbetarpulsen.Core.BusinessLogic.Data;
using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.DataModels.FormModels;
using Medarbetarpulsen.Core.DataModels.ViewModels;
using Medarbetarpulsen.Core.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Medarbetarpulsen.Website.Controllers.Developer
{
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        // Returns the three latest forms complete with details
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult Get(int organizationId)
        {
            var dataForOrganization = DataHelper.Get(organizationId);

            if (dataForOrganization != null)
            {
                return Ok(dataForOrganization);
            }

            return NotFound();
        }

        //Sends a reminder to employees who haven't answered the form yet
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult SendReminder([FromBody] WrapperViewModel wrapper)
        {
            if(FormHelper.SendReminder(wrapper))
            {
                return Ok();
            }

            return BadRequest("Couldn't send reminder to employees. Something went wrong");
        }

        //NOTE: Is this being used?
        // Get all departments for the organization
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult GetDepartments(int organizationId)
        {
            if(organizationId != 0)
            {
                var departments = DbAccess.GetDepartmentsForOrganization(organizationId);

                if(departments.Count() != 0)
                {
                    return Ok(departments);
                }
            }

            return NotFound();
        }

        // Returns a list of deparments with employees for each department
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult GetDepartmentsAndEmployees(int organizationId)
        {
            if(organizationId != 0)
            {
                var listOfDepartmentsWithEmployees = EmployeeHelper.MatchEmployeesWithDepartment(organizationId);

                if(listOfDepartmentsWithEmployees.Any())
                {
                    return Ok(listOfDepartmentsWithEmployees);
                }
            }

            return NotFound();
        }

        //NOTE: Is this being used?
        //Show all users working at a specific department
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult EmployeesWith(int departmentId)
        {
            if (departmentId > 0)
            {
                var employees = DbAccess.GetEmployeesByDepartment(departmentId)?.ToList();
                return Ok(employees);
            }
            return BadRequest("Error Getting employees from Department with requested ID");
        }

        //Creates a new employee
        [EnableCors("*", "*", "*")]
        [HttpPost]
        public IHttpActionResult AddEmployee([FromBody] Employee employee)
        {
            if(EmployeeHelper.Create(employee))
            {
                return Ok(employee);
            }

            return BadRequest("Couldn't create the requested employee, something went wrong.");
        }

        //Updates the employee
        [EnableCors("*", "*", "*")]
        [HttpPost]
        public IHttpActionResult UpdateEmployee([FromBody] Employee employee)
        {
            if(EmployeeHelper.Update(employee))
            {
                return Ok(employee);
            }

            return BadRequest("Couldn't perform the requested action, something went wrong");
        }
 
        //Delete user(s) from the Employees table
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult DeleteEmployee(string emailAddress)
        {
            if(EmployeeHelper.Delete(emailAddress))
            {
                return Ok();
            }

            return NotFound();
        }
    }
}


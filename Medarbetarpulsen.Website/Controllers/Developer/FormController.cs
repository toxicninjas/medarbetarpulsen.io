﻿using Medarbetarpulsen.Core.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.DataModels.ViewModels;
using Medarbetarpulsen.Core.Enums;
using Medarbetarpulsen.Core.DataModels.FormModels;
using Medarbetarpulsen.Core.Tokens;

namespace Medarbetarpulsen.Website.Controllers.Developer
{
    [RoutePrefix("api/form")]
    public class FormController : ApiController
    {
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult Test()
        {
            var token = GenerateFormToken.Generate();

            return Ok(token);
        }

        //Creates new form and saves to the database
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult Create([FromBody] FormViewModel form)
        {
            if(FormHelper.Create(form))
            {
                return Ok(form);
            }

            return BadRequest("Couldn't create form, something went wrong");
        }

        //Submit formanswers and update database
        [EnableCors("*", "*", "*")]
        [HttpPost]
        public IHttpActionResult SubmitForm([FromBody] SubmitFormViewModel form)
        {
            if(FormHelper.Submit(form))
            {
                return Ok(form);
            }
            
            return BadRequest("Something went wrong when submitting the form");
        }

        //Create new form question(dynamically)
        [EnableCors("*", "*", "*")]
        [HttpPost]
        public IHttpActionResult CreateQuestion([FromBody] FormQuestion model)
        {
            var newQuestionIsCreated = DbAccess.CreateNewFormQuestion(model.Question, model.FormQuestionTypeId, model.FormId);

            if (newQuestionIsCreated)
            {
                return Ok();
            }

            return BadRequest("Couldn't perform the requested action, something went wrong");
        }

        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult GetForm(int formId)
        {
            var form = DbAccess.GetCompleteForm(formId);

            if(form != null)
            {
                return Ok(form);
            }

            return BadRequest("Couldn't perform the requested action, something went wrong");
        }

        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult ValidateFormToken(string formToken)
        {
            var form = FormHelper.ValidateToken(formToken);

            if(form != null)
            {
                return Ok(form);
            }

            return NotFound();
        }

        //TODO: Should add attribute to send email from employer
        [EnableCors("*", "*", "*")]
        [HttpPost]
        public IHttpActionResult SendForm([FromBody] SendFormViewModel form)
        {
            if (FormHelper.Send(form))
            {
                return Ok(form);
            }

            return BadRequest("Couldn't send email, something went wrong");
        }


        // Retrieves complete data for the given form. 
        [EnableCors("*", "*", "*")]
        [HttpGet]
        public IHttpActionResult GetFormData([FromBody] FormDataViewModel form)
        {
            var formData = FormHelper.Get(form.FormId, form.EditionId, form.DepartmentId);

            if(formData != null)
            {
                return Ok(formData);
            }

            return NotFound();
        }
    }
}

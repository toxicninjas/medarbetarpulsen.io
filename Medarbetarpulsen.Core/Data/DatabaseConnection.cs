﻿using Toxic.Data;
using System.Configuration;

namespace Medarbetarpulsen.Core.Data
{
    public static class DatabaseConnection
    {
        public static RdbContext Context
        {
            get
            {
                var connection = ConfigurationManager.ConnectionStrings["DatabaseConnection"]?.ConnectionString;

                return new RdbContext(new SqlDataSource(connection));
            }
        }
    }
}

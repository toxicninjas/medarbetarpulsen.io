﻿using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.BusinessLogic;
using System;
using Toxic.Core;
using Toxic.Data;

namespace Medarbetarpulsen.Core.Authentication
{
    internal class UserData
    {
        private static RdbContext _db = null;

        private static RdbContext Db
        {
            get
            {
                if (_db == null)
                {
                    _db = DbContext.Db;
                }
                return _db;
            }
        }

        internal static User GetUserById(int id)
        {
            var result = Db.ExecuteRecord<User>("SELECT TOP 1 * FROM Users WHERE Id = {0}", id);

            return result != null && !result.DateDeleted.HasValue ? result : null;
        }

        internal static User GetUserByEmail(string emailAddress, bool evenDeleted = false)
        {
            emailAddress = emailAddress.Clean();

            if (string.IsNullOrEmpty(emailAddress))
            {
                return null;
            }

            var result = Db.ExecuteRecord<User>("SELECT TOP 1 * FROM Users WHERE Email = {0}", emailAddress);

            return result != null && (evenDeleted || !result.DateDeleted.HasValue) ? result : null;
        }

        internal static bool SaveUser(User identity)
        {
            return Db.UpsertRecord("Users", identity);           
        }

        internal static UserPasswordResetToken GetPasswordResetToken(string emailAddress)
        {
            return Db.ExecuteRecord<UserPasswordResetToken>("SELECT TOP 1 * FROM UserPasswordResetTokens WHERE EmailAddress = {0}", emailAddress);        
        }

        internal static UserPasswordResetToken GetPasswordResetToken(int token)
        {
            return Db.ExecuteRecord<UserPasswordResetToken>("SELECT TOP 1 * FROM UserPasswordResetTokens WHERE token = {0}", token);          
        }

        internal static bool SavePasswordResetToken(UserPasswordResetToken resetToken)
        {
            return Db.UpsertRecord("UserPasswordResetTokens", resetToken);        
        }
    }
}

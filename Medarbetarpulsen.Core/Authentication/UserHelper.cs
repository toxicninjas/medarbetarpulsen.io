﻿using System;
using System.Security.Cryptography;
using Toxic.Core;
using Toxic.Data;
using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.Enums;
using Medarbetarpulsen.Core.BusinessLogic;

namespace Medarbetarpulsen.Core.Authentication
{
    internal class UserHelper
    {
        private static Salt GenerateSalt(int size = 16)
        {
            var cryptoProvider = new RNGCryptoServiceProvider();

            var saltBytes = new Byte[size];
            cryptoProvider.GetNonZeroBytes(saltBytes);

            return new Salt { SaltString = Convert.ToBase64String(saltBytes), SaltBytes = saltBytes };
        }

        private static string HashPassword(string password, Salt salt)
        {
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt.SaltBytes, 10000);
            var hashPassword = Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256));

            return hashPassword;
        }      

        private static bool VerifyPassword(string enteredPassword, string storedHash, string storedSalt)
        {
            enteredPassword = enteredPassword.Clean();
            storedHash = storedHash.Clean();
            storedSalt = storedSalt.Clean();

            if (enteredPassword == null || storedHash == null || storedSalt == null)
            {
                return false;
            }

            var saltBytes = Convert.FromBase64String(storedSalt);
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(enteredPassword, saltBytes, 10000);      

            var result = Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256)) == storedHash;

            return result;
        }

        // Note: We overwrite existing if it is marked as deleted, but throw an exception
        // if it is not.
        internal static User CreateUser(string emailAddress, string organizationName, string password = null)
        {
            emailAddress = emailAddress.Clean();
            password = password.Clean();
            organizationName = organizationName.Clean();

            if (emailAddress == null)
            {
                throw new ArgumentException("E-mail address cannot be null", nameof(emailAddress));
            }

            if (organizationName == null)
            {
                throw new ArgumentException("Organization name cannot be null", nameof(organizationName));
            }

            var organization = DbAccess.GetOrganizationByName(organizationName);

            if(organization == null)
            {
                //TODO: Felhantering up in this
                DbAccess.CreateNewOrganization(organizationName);
            }

            var user = UserData.GetUserByEmail(emailAddress);

            if (user != null && user.DateDeleted != null)
            {
                throw new Exception($"The employer ({emailAddress}) already existed");
            }

            var salt = password != null ? GenerateSalt() : null;
            var hash = password != null ? HashPassword(password, salt) : null;

            user = new User
            {
                Id = 1,
                Salt = salt?.SaltString,
                Email = emailAddress,
                Hash = hash,
                DateCreated = DateTime.Now,
                UserTypeId = (int)Enums.UserType.User,
                OrganizationId = organization.Id
            };

            if (!UserData.SaveUser(user))
            {
                throw new Exception($"Could not create User. Something went wrong");
            }

            return user;
        }

        internal static User Login(string emailAddress, string password)
        {
            if(!string.IsNullOrEmpty(emailAddress) &&
               !string.IsNullOrEmpty(password))
            {
                var user = UserData.GetUserByEmail(emailAddress);
                if (user != null && VerifyPassword(password, user.Hash, user.Salt))
                {
                    return user;
                }
            }
            
            return null;
        }

        internal static string CreateResetToken(string emailAddress, DateTime? expiresWhen)
        {
            int token = 0;

            // TODO: This is wrong:
            if(UserData.GetPasswordResetToken(emailAddress) == null)
            {
                var employer = UserData.GetUserByEmail(emailAddress);

                if(employer != null)
                {
                    token = GenerateResetToken();
                    var rng = new Random();
                    var randomId = rng.Next(10000000);

                    var resetToken = new UserPasswordResetToken
                    {
                        Id = randomId,
                        UserId = employer.Id,
                        EmailAddress = employer.Email,
                        Token = token,
                        ExpiresWhen = expiresWhen.HasValue ? expiresWhen.Value : DateTime.Now.AddHours(24)
                    };

                    if (!UserData.SavePasswordResetToken(resetToken))
                    {
                        throw new Exception($"Could not create Employer password reset token. Something went wrong");
                    }                    
                }
            }

            return token != 0 ? token.ToString() : null;
        }

        internal static User CheckResetToken(int token)
        {
            if (token != 0)
            {
                var resetToken = UserData.GetPasswordResetToken(token);

                if (resetToken != null && !resetToken.Expired && !resetToken.TokenUsed)
                {
                    // TODO: Maybe this is too soon?
                    resetToken.TokenUsed = true;

                    if (UserData.SavePasswordResetToken(resetToken))
                    {
                        return UserData.GetUserById(resetToken.UserId);
                    }
                }
            }

            return null;
        } 

        internal static User SetPassword(int id, string password)
        {
            var employer = UserData.GetUserById(id);

            if (employer != null)
            {
                password = password.Clean();

                var salt = password != null ? GenerateSalt() : null;
                var hash = password != null ? HashPassword(password, salt) : null;

                employer.Salt = salt?.SaltString;
                employer.Hash = hash;

                UserData.SaveUser(employer);

                return employer;
            }
            else
            {
                throw new Exception($"Could not find and update password for Employer with id {id}");
            }
        }

        internal static bool DeleteUser(int id)
        {
            bool deleted = false;
            var employer = UserData.GetUserById(id);
            
            if(employer != null)
            {
                employer.DateDeleted = DateTime.Now;
                deleted = UserData.SaveUser(employer);
            }            

            return deleted;
        }

        private static int GenerateResetToken()
        {
            var rng = new Random();
            var token = rng.Next(1000000);
            return token;
        }        
    }
}

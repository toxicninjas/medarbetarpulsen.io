﻿using Medarbetarpulsen.Core.DataModels;
using System;

namespace Medarbetarpulsen.Core.Authentication
{
    public class UserManager
    {	
		// returnerar Identity (null om den redan fanns)
		public User CreateUser(string email, string organizationName, string password)
		{
			return UserHelper.CreateUser(email, organizationName, password);
		}

		public User GetUserById(int id)
        {
			return UserData.GetUserById(id);
        }

		public User Login(string email, string password)
		{
			return UserHelper.Login(email, password);			
		}

		public bool DeleteUser(int id)
		{
			return UserHelper.DeleteUser(id);
		}
		
		public string CreateResetToken(string email, DateTime? expiresWhen = null)
		{
			return UserHelper.CreateResetToken(email, expiresWhen);
		}

		public User CheckResetToken(int token)
		{
			return UserHelper.CheckResetToken(token);
		}
	
		public User SetPassword(int id, string password)
		{
			return UserHelper.SetPassword(id, password);
		}
	}
}

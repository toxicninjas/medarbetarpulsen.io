﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.Authentication
{
    internal class Salt
    {
        internal string SaltString { get; set; }
        internal byte[] SaltBytes { get; set; }
    }
}

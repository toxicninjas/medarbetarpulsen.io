﻿using Medarbetarpulsen.Core.DataModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.Tokens
{
    public static class TokenHelper
    {
        public static List<EmployeeFormAnswerHistory> Populate(SendFormViewModel form, 
                                                                List<EmployeeViewModel> employees, 
                                                                FormQuestionViewModel edition)
        {
            var tokenList = new List<EmployeeFormAnswerHistory>();

            foreach (var employee in employees)
            {
                var tokenModel = new EmployeeFormAnswerHistory()
                {
                    Token = GenerateFormToken.Generate(),
                    EditionId = edition.EditionId,
                    DateTokenExpired = DateTime.Now.AddDays(7),
                    FormId = form.FormId,
                    EmailAddress = employee.EmailAddress,
                    EmployeeId = employee.Id
                };
                
                tokenList.Add(tokenModel);
            }

            return tokenList.Any() ? tokenList : null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.Tokens
{
    //TODO: Should probably rename this
    public class EmployeeFormAnswerHistory
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public int EditionId { get; set; }
        public int FormId { get; set; }
        public int EmployeeId { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateTokenExpired { get; set; }
        public DateTime? DateAnswered { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels
{
    public class LogMessage
    {
        public List<string> Info{ get; set; }
        public List<string> Warnings { get; set; }
        public List<string> Errors { get; set; }

    }
}

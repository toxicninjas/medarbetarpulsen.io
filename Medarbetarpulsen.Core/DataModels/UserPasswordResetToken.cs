﻿using System;

namespace Medarbetarpulsen.Core.DataModels
{
    internal class UserPasswordResetToken
    {
        internal int Id { get; set; }
        internal int UserId { get; set; }
        internal string EmailAddress { get; set; }
        internal int Token { get; set; }
        internal DateTime ExpiresWhen { get; set; }
        internal bool TokenUsed { get; set; }

        internal bool Expired
        {
            get
            {
                return DateTime.Compare(ExpiresWhen, DateTime.Now) < 0 ? true : false;
            }            
        }
    }
}

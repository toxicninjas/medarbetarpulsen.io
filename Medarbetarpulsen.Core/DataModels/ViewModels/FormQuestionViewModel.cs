﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class FormQuestionViewModel
    {
        public int FormId { get; set; }
        public string Question { get; set; }
        public string QuestionTitle { get; set; }
        public int FormQuestionTypeId { get; set; }
        public int EditionId { get; set; }
        public int OrganizationId { get; set; }
    }
}

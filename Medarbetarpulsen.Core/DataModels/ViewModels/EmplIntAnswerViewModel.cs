﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class EmplIntAnswerViewModel
    {
        public string Question { get; set; }
        public string QuestionTitle { get; set; }
        public int? IntAnswer { get; set; }
        public int Max { get; set; }
        public int Min { get; set; }
        public double Avg { get; set; }
    }
}

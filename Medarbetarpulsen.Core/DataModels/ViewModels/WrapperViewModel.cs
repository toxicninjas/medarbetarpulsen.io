﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class WrapperViewModel
    {
        public List<EmplIntAnswerViewModel> IntAnswers = new List<EmplIntAnswerViewModel>();
        public List<EmplTextAnswerViewModel> TextAnswers = new List<EmplTextAnswerViewModel>();

        public FormAnswerRateViewModel AnswerRate = new FormAnswerRateViewModel();
    }
}

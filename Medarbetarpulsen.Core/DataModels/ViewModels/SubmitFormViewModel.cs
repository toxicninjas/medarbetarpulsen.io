﻿using Medarbetarpulsen.Core.DataModels.FormModels;
using Medarbetarpulsen.Core.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class SubmitFormViewModel
    {
        public FormViewModel Form { get; set; }

        public List<FormAnswer> Answers { get; set; }

        public EmployeeFormAnswerHistory Token { get; set; }
    }
}

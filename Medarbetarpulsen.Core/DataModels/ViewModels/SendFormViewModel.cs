﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class SendFormViewModel
    {
        public DepartmentViewModel Department { get; set; }
        public int FormId { get; set; }
    }
}

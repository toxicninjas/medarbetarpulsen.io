﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class EmplTextAnswerViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress{ get; set; }
        public string Question { get; set; }
        public string QuestionTitle { get; set; }
        public string TextAnswer { get; set; }
    }
}

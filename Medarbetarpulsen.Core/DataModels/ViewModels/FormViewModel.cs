﻿using Medarbetarpulsen.Core.DataModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.FormModels
{
    public class FormViewModel
    {
        public int Id { get; set; }
        public int EditionId { get; set; }
        public int OrganizationId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public List<FormQuestionViewModel> Questions = new List<FormQuestionViewModel>();
    }
}

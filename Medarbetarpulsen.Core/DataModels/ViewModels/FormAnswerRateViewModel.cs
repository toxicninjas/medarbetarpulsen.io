﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class FormAnswerRateViewModel
    {
        public List<Employee> EmployeesAnswered = new List<Employee>();
        public List<Employee> EmployeesNotAnswered = new List<Employee>();
    }
}

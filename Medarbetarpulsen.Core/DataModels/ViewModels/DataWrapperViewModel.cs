﻿using Medarbetarpulsen.Core.DataModels.FormModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels.ViewModels
{
    public class DataWrapperViewModel
    {
       public List<FormViewModel> Forms = new List<FormViewModel>();
       public List<DepartmentViewModel> Departments = new List<DepartmentViewModel>();
       public List<WrapperViewModel> FormsStatistic = new List<WrapperViewModel>();
    }
}

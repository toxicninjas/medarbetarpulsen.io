﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Salt { get; set; }
        public string Hash { get; set; }
        public int UserTypeId { get; set; }
        public int OrganizationId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; } = null;
        public DateTime? DateUpdated { get; set; } = null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels
{
    public class FormAnswer
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public int FormQuestionId { get; set; }
        public int FormQuestionTypeId { get; set; }
        public int EmployeeId { get; set; }
        public int IntAnswer { get; set; }
        public string StringAnswer { get; set; }
        public int EditionId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateChanged { get; set; }
        public DateTime DateDeleted { get; set; }
    }
}

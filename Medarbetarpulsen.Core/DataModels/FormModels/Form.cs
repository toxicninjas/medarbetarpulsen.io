﻿using Medarbetarpulsen.Core.DataModels.FormModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels
{
    public class Form
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public string Name { get; set; }

        public List<FormEdition> Editions = new List<FormEdition>();
        public int LatestEdition { get; set; }
        public string Description { get; set; }
        public string DepartmentName { get; set; }
        public int AnswerCount { get; set; }
        public int TotalCount { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}

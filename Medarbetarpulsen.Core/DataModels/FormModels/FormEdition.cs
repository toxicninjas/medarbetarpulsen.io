﻿using System;

namespace Medarbetarpulsen.Core.DataModels.FormModels
{
    public class FormEdition
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public int EditionNumber { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}
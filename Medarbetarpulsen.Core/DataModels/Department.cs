﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.DataModels
{
    public class Department
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toxic.Data;
using Medarbetarpulsen.Core.Data;
using Medarbetarpulsen.Core.DataModels;

namespace Medarbetarpulsen.Core.BusinessLogic
{
    public class DbContext
    {
        //DB
        private static RdbContext _db;
        public static RdbContext Db
        {
            get
            {
                if (_db == null)
                {
                    _db = DatabaseConnection.Context;
                }
                return _db;
            }
        }

        // This is just for test :)
        public static List<Language> GetLanguages()
        {
            return Db.ExecuteRecords<Language>("select * from Languages")?.ToList();
        }
    }
}

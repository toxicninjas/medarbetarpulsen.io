﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MailKit;
using MailKit.Net.Smtp;
using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.Tokens;
using MimeKit;
using RestSharp;
using RestSharp.Authenticators;

namespace Medarbetarpulsen.Core.BusinessLogic
{
    public static class EmailHelper
    {
        //TODO: This needs additional work
        public static bool SendReminder(string senderEmail, List<EmployeeFormAnswerHistory> tokenList)
        {
            if(tokenList.Any())
            {
                List<MimeMessage> emails = new List<MimeMessage>();

                foreach(var token in tokenList)
                {
                    MimeMessage mail = new MimeMessage();

                    mail.From.Add(new MailboxAddress("Toxic Interactive Solutions", senderEmail));
                    mail.To.Add(new MailboxAddress("Den bästa medarbetaren", token.EmailAddress));

                    mail.Subject = "Påminnelse";

                    mail.Body = new TextPart("plain")
                    {
                        Text = @"Hej! Detta är en påminnelse till dig att svara på formuläret nedan!" +
                        @"medarbetarpulsen.io/form/" + token.Token
                    };

                    emails.Add(mail);
                }

                try
                {
                    using (var client = new SmtpClient())
                    {
                        // XXX - Should this be a little different?
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        client.Connect("smtp.eu.mailgun.org", 587, false);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("postmaster@mg.toxicmail.se", "bc5177a51fc0e636eba9d10ab29f19c8-360a0b2c-d20063cb");

                        foreach (var email in emails)
                        {
                            client.Send(email);
                        }

                        client.Disconnect(true);
                    }

                    return true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
                return false;
        }

        //TODO: Implement function to see which mails doesn't go through, or something
        public static bool SendFormToEmployees(string senderEmail, List<EmployeeFormAnswerHistory> tokenList)
        {
            // Compose a message
            List<MimeMessage> emails = new List<MimeMessage>();
            foreach(var token in tokenList)
            {
                MimeMessage mail = new MimeMessage();
                //TODO: Change this to be dynamic
                mail.From.Add(new MailboxAddress("Toxic Interactive Solutions", senderEmail));
                mail.To.Add(new MailboxAddress("Den bästa medarbetaren", token.EmailAddress));

                mail.Subject = "Hello";
                
                    mail.Body = new TextPart("plain")
                    {
                        Text = @"medarbetarpulsen.io/form/" + token.Token
                    };

                emails.Add(mail);  
            }
            
            // Send it!
            try
            {
                using (var client = new SmtpClient())
                {
                    // XXX - Should this be a little different?
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.eu.mailgun.org", 587, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate("postmaster@mg.toxicmail.se", "bc5177a51fc0e636eba9d10ab29f19c8-360a0b2c-d20063cb");

                    foreach(var email in emails)
                    {
                        client.Send(email);
                    }

                    client.Disconnect(true);
                }

                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return false;
        }
    }
}

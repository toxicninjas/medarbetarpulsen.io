﻿using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.DataModels.FormModels;
using Medarbetarpulsen.Core.DataModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.BusinessLogic.Data
{
    public class DataHelper
    {
        public static LiteDashBoardViewModel Get(int organizationId)
        {
            //gets the 3 latest forms for display on dashboard
            var forms = DbAccess.GetFormsForOrganization(organizationId)?.Take(3);
            var departments = DbAccess.GetDepartmentsForOrganization(organizationId);
            var formEditions = DbAccess.GetFormEditions()?.OrderBy(x => x.DateCreated)?.ToList();

            var formData = GetFormData(forms, departments, formEditions);

            return formData != null ? formData : null;
        }

        internal static LiteDashBoardViewModel GetFormData(IEnumerable<Form> forms, List<Department> departments, List<FormEdition> formEditions)
        {
            var dashboardModel = new LiteDashBoardViewModel();

            foreach (var form in forms)
            {
                form.Editions = formEditions.Where(x => x.FormId == form.Id)?.ToList();
                form.LatestEdition = form.Editions.OrderByDescending(x => x.DateCreated)?.Select(x => x.Id).FirstOrDefault() ?? 0;

                //This seems weird?
                if (form.Editions != null && form.Editions.Any())
                {
                    foreach (var department in departments)
                    {
                        var employeeCountForDepartment = DbAccess.GetEmployeesByDepartment(department.Id)?.Count() ?? 0;
                        var formDepartmentEdition = form.Editions.FirstOrDefault(x => x.DepartmentId == department.Id);
                        
                        if (formDepartmentEdition != null)
                        {
                            form.DepartmentName = department.Name;
                            var formAnswerCount = DbAccess.GetAnsweredEmployees(form.Id, form.LatestEdition, department.Id).Count();

                            if (formAnswerCount != 0 && employeeCountForDepartment != 0)
                            {
                                form.AnswerCount = formAnswerCount;
                                form.TotalCount = employeeCountForDepartment;
                            }
                        }
                    }

                    dashboardModel.Forms.Add(form);
                }
            }

            return dashboardModel.Forms.Any() ? dashboardModel : null;
        }
    }
}

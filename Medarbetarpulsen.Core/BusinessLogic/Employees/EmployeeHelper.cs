﻿using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.DataModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.BusinessLogic
{
    public static class EmployeeHelper
    {
        public static bool Create(Employee employee)
        {
            if (employee != null)
            {
                var createdEmployee = DbAccess.CreateEmployee(employee.FirstName, employee.LastName,
                                                                employee.EmailAddress, employee.DepartmentId);

                if (!string.IsNullOrEmpty(createdEmployee.EmailAddress))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool Delete(string emailAddress)
        {
            var deletedEmployee = DbAccess.DeleteEmployee(emailAddress);

            if (deletedEmployee != null)
            {
                return true;
            }

            return false;
        }

        public static bool Update(Employee employee)
        {
            if (!string.IsNullOrEmpty(employee.EmailAddress) &&
               !string.IsNullOrEmpty(employee.FirstName) &&
               !string.IsNullOrEmpty(employee.LastName) &&
               employee.DepartmentId != 0)
            {
                var employeeIsUpdated = DbAccess.UpdateEmployee(employee.Id, employee.FirstName, employee.LastName, employee.EmailAddress, employee.DepartmentId);

                if (employeeIsUpdated != null)
                {
                    return true;
                }
            }

            return false;
        }

        public static List<DepartmentViewModel> MatchEmployeesWithDepartment(int organizationId)
        {
            var departmentViewmodels = new List<DepartmentViewModel>();

            var departments = DbAccess.GetDepartmentsForOrganization(organizationId);
            var allEmployees = DbAccess.GetAllEmployeesByOrganization(organizationId);

            if (departments.Count != 0 && allEmployees.Count != 0)
            {
                foreach (var department in departments)
                {
                    var departmentEmployees = allEmployees.Where(x => x.DepartmentId == department.Id)?.ToList();

                    var employeeViewModels = new List<EmployeeViewModel>();
                    foreach (var employee in departmentEmployees)
                    {
                        employeeViewModels.Add(new EmployeeViewModel()
                        {
                            Id = employee.Id,
                            FirstName = employee.FirstName,
                            LastName = employee.LastName,
                            EmailAddress = employee.EmailAddress,
                            DepartmentId = employee.DepartmentId
                        });
                    }
                    departmentViewmodels.Add(new DepartmentViewModel()
                    {
                        Id = department.Id,
                        Name = department.Name,
                        OrganizationId = department.OrganizationId,
                        Employees = employeeViewModels
                    });
                }

                return departmentViewmodels;
            }

            return null;
        }
    }
}

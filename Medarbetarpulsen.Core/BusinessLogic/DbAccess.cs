﻿using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.DataModels.FormModels;
using Medarbetarpulsen.Core.DataModels.ViewModels;
using Medarbetarpulsen.Core.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toxic.Core;
using Toxic.Data;

namespace Medarbetarpulsen.Core.BusinessLogic
{
    public static class DbAccess
    {
        private static RdbContext _db = null;

        private static RdbContext Db
        {
            get
            {
                if (_db == null)
                {
                    _db = DbContext.Db;
                }
                return _db;
            }
        }

        #region Organization/Departmens
        public static List<Department> GetDepartmentsForOrganization(int organizationId)
        {
            return Db.ExecuteRecords<Department>("SELECT * FROM Departments WHERE OrganizationId={0}", organizationId)?.ToList() ?? new List<Department>();
        }

        public static List<Employee> GetAllEmployeesByOrganization(int organizationId)
        {
            return Db.ExecuteRecords<Employee>("SELECT e.id, e.firstName, e.lastName, e.emailaddress, e.departmentId FROM Departments as d " +
                                               "INNER JOIN Employees as e on e.departmentId = d.Id " +
                                               "WHERE d.organizationId={0} " +
                                               "AND DateDeleted IS NULL", organizationId)?.ToList();
        }

        public static Organization GetOrganizationByName(string organizationName)
        {
            var result = Db.ExecuteRecord<Organization>("SELECT TOP 1 * FROM Organizations WHERE Name = {0}", organizationName);

            return result != null ? result : null;
        }

        public static Department CheckIfDepartmentExistsInOrganization(string departmentName, int organizationId)
        {
            var result = Db.ExecuteRecord<Department>("SELECT TOP 1 * FROM Departments WHERE Name = {0} AND OrganizationId = {1}", departmentName, organizationId);

            return result != null ? result : null;
        }

        public static bool CreateNewOrganization(string organizaitonName)
        {
            return Db.UpsertRecord("Organizations", organizaitonName);
        }

        public static Department CreateNewDepartment(string departmentName, string userEmail)
        {
            departmentName = departmentName.Clean();
            userEmail = userEmail.Clean();

            var user = Db.ExecuteRecord<User>("SELECT TOP 1 * FROM Users WHERE Email = {0} AND DateDeleted IS NULL", userEmail);
            var departmentMatches = CheckIfDepartmentExistsInOrganization(departmentName, user.OrganizationId);

            if(departmentMatches != null)
            {
                throw new ArgumentException("Department does already exist ", nameof(departmentName));
            }

                var department = new Department()
                {
                    OrganizationId = user.OrganizationId,
                    Name = departmentName
                };

                if(!SaveDepartment(department))
                {
                    throw new ArgumentException("Error creating department ", nameof(department));
                }
            return department;
        }

        public static bool SaveDepartment(Department department)
        {
            return Db.UpsertRecord("Departments", department);
        }

        #endregion

        #region EMAIL
        public static bool CreateNewEmailList(List<Employee> employees)
        {
            bool result = false;           

            if (employees.Count > 0)
            {
                result = Db.UpsertRecords("Employees", employees);
            }               

            return result;
        }

        #endregion
        
        #region EMPLOYEES
        public static Employee CreateEmployee(string firstName, string lastName, string emailAddress, int departmentId)
        {
            
            var employee = new Employee()
            {
                FirstName = firstName,
                LastName = lastName,
                EmailAddress = emailAddress,
                DepartmentId = departmentId,
                DateCreated = DateTime.Now
            };

            if(SaveEmployee(employee))
            {
                return employee;
            }

            return new Employee();
        }

        public static Employee UpdateEmployee(int id, string firstName, string lastName, string emailAddress, int departmentId)
        {
            var employee = GetEmployeeById(id);

            if(employee != null)
            {
                employee.FirstName = firstName;
                employee.LastName = lastName;
                employee.EmailAddress = emailAddress;
                employee.DepartmentId = departmentId;
                employee.DateUpdated = DateTime.Now;

                if(SaveEmployee(employee))
                {
                    return employee;
                }
            }

            return new Employee();
        }

        public static List<Employee> GetEmployeesByDepartment(int departmentId)
        {
            return Db.ExecuteRecords<Employee>("SELECT * FROM Employees WHERE DepartmentId = {0} AND DateDeleted IS Null", departmentId)?.ToList() ?? new List<Employee>();
        }

        public static Employee DeleteEmployee(string emailAddress)
        {
            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                emailAddress = emailAddress.Clean();
                var employee =  GetEmployeeByEmail(emailAddress);
                if(employee != null)
                {
                    employee.DateUpdated = DateTime.Now;
                    employee.DateDeleted = DateTime.Now;

                    if(SaveEmployee(employee))
                    {
                        return employee;
                    }
                }
            }
            return new Employee();
        }

        public static Employee GetEmployeeById(int employeeId)
        {
            var employee = Db.ExecuteRecord<Employee>("SELECT TOP 1 * FROM Employees WHERE Id = {0}", employeeId);

            return employee != null ? employee : null;
        }

        public static Employee GetEmployeeByEmail(string emailAddress)
        { 
            var user = Db.ExecuteRecord<Employee>("SELECT TOP 1 * FROM Employees WHERE EmailAddress = {0} " +
                                                  "AND DateDeleted IS NULL" , emailAddress);

            return user;
        }

        public static bool SaveEmployee(Employee employee)
        {
            return Db.UpsertRecord("Employees", employee);
        }
        #endregion

        #region FORMS

        public static EmployeeFormAnswerHistory ValidateFormToken(string token)
        {
            var formToken = Db.ExecuteRecord<EmployeeFormAnswerHistory>("SELECT TOP 1 * FROM EmployeeFormAnswersHistory Where Token = {0}", token);

            if(formToken?.DateTokenExpired > DateTime.Now && formToken != null)
            {
                return formToken;
            }

            return null;
        }

        public static List<FormEdition> GetFormEditions()
        {
            var formEditions = Db.ExecuteRecords<FormEdition>("SELECT * FROM FormEditions").ToList();

            return formEditions;
        }

        public static Form GetFormById(int formId)
        {
            var form = Db.ExecuteRecord<Form>("SELECT TOP 1 * FROM Forms WHERE Id = {0} AND DateDeleted IS NULL", formId);

            if(form != null)
            {
                return form;
            }

            return new Form();
        }

        public static List<Form> GetFormsForOrganization(int organizationId)
        {
            var forms = Db.ExecuteRecords<Form>("SELECT * FROM Forms WHERE OrganizationId = {0} ORDER BY DateCreated", organizationId)?.ToList() ?? new List<Form>();

            return forms;
        }

        public static bool CreateForm(FormViewModel form)
        {
            if (!string.IsNullOrEmpty(form.Name) && form.OrganizationId != 0)
            {
                 var newForm = new Form();

                 //Should this be here? I dunno
                 newForm.Id = GenerateRandomFormId();
                 newForm.Name = form.Name;
                 newForm.Description = form.Description;
                 newForm.OrganizationId = form.OrganizationId;
                 newForm.DateCreated = DateTime.Now;

                return SaveForm(newForm) ? true : false;
            }

            return false;
        }

        public static List<FormQuestion> GetFormQuestions(int formId, int editionId)
        {
           if(formId != 0 && editionId != 0)
           {
               var questions = Db.ExecuteRecords<FormQuestion>("SELECT * FROM FormQuestions WHERE FormId = {0} AND EditionId = {1}", formId, editionId)?.ToList() ?? new List<FormQuestion>();

               return questions;
           }
           return new List<FormQuestion>();
        }

        public static FormQuestionViewModel GetEditionId(int formId)
        {
            var edition = Db.ExecuteRecord<FormQuestionViewModel>("SELECT fq.EditionId FROM FormQuestions as fq " +
                                                                     "INNER JOIN Forms as f on f.id = fq.FormId WHERE FormId = {0} " +
                                                                     "AND DateDeleted IS NULL", formId);
            return edition != null ? edition : null;
        }

        public static FormViewModel GetCompleteForm(int formId)
        {
            var questions = Db.ExecuteRecords<FormQuestionViewModel>("SELECT fq.Question, fq.FormId, fq.FormQuestionTypeId, fq.EditionId, fq.QuestionTitle, f.Name, f.OrganizationId  FROM FormQuestions as fq " +
                                                        "INNER JOIN Forms as f on f.id = fq.FormId " +
                                                        "WHERE FormId = {0} AND f.DateDeleted IS NULL", formId).ToList();

            //TODO: This should be reworked
            var edition = GetEditionId(formId);
            
            var form = GetFormById(formId);
            if (questions.Count != 0 && form != null)
            {
                var formViewModel = new FormViewModel()
                {
                    EditionId = edition.EditionId,
                    OrganizationId = form.OrganizationId,
                    Name = form.Name
                };

                foreach(var question in questions)
                {
                    formViewModel.Questions.Add(question);
                }

                return formViewModel;
            }

            return new FormViewModel();
        }

        public static List<FormAnswer> GetFormAnswers(int formId, int editionId)
         {
            List<FormAnswer> answers  = new List<FormAnswer>();
            if(formId != 0 && editionId != 0)
            {
                var formAnswers = Db.ExecuteRecords<FormAnswer>("SELECT * FROM FormAnswers WHERE FormId = {0} AND EditionId = {1} " +
                                                                "ORDER BY DateCreated", formId, editionId)?.ToList() ?? new List<FormAnswer>();
                return formAnswers;
            }

            return answers;
         }

        public static List<Employee> GetAnsweredEmployees(int formId, int editionId, int departmentId)
        {
            if (formId != 0 && editionId != 0 && departmentId != 0)
            {
                var employeesWhoAnswered = Db.ExecuteRecords<FormAnswer>("SELECT DISTINCT EmployeeId FROM FormAnswers WHERE FormId = {0} AND EditionId = {1}", formId, editionId)?.ToList() ?? new List<FormAnswer>();

                if (employeesWhoAnswered.Any())
                {
                    var employees = new List<Employee>();

                    var allEmployees = GetEmployeesByDepartment(departmentId);

                    if(allEmployees.Any())
                    {
                        foreach (var entity in employeesWhoAnswered)
                        {
                            //var employee = GetEmployeeById(entity.EmployeeId);

                            var employee = allEmployees.FirstOrDefault(x => x.Id == entity.EmployeeId);

                            if (employee != null)
                            {
                                employees.Add(employee);
                            }
                        }

                        return employees;
                    }
                }
            }

            return new List<Employee>();
        }

        public static List<Employee> GetUnansweredEmployees(int formId, int editionId, int departmentId)
        {
            List<Employee> unAnsweredEmployees = new List<Employee>();

            if (formId != 0 && editionId != 0 && departmentId != 0)
            {
                var existingEmployeesList = GetEmployeesByDepartment(departmentId);
                var employeesWhoAnswered = GetAnsweredEmployees(formId, editionId, departmentId);

                if(existingEmployeesList.Any() && employeesWhoAnswered.Any())
                {    
                    foreach (var employee in existingEmployeesList)
                    {
                        if(!employeesWhoAnswered.Any(x => x.EmailAddress == employee.EmailAddress))
                        {
                            unAnsweredEmployees.Add(employee);
                        }
                    }                   
                }
            }

            return unAnsweredEmployees;
        }

        public static bool CreateNewFormQuestion(string question, int questionTypeId, int formId)
        {
            if (!string.IsNullOrEmpty(question) && questionTypeId != 0 && formId != 0)
            {
                var newQuestion = new FormQuestion()
                {
                    FormId = formId,
                    Question = question,
                    FormQuestionTypeId = questionTypeId,
                    EditionId = 1
                };              

                if (SaveQuestion(newQuestion))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool SaveQuestion(FormQuestion formQuestion)
        {
            return Db.UpsertRecord("FormQuestions", formQuestion);
        }

        public static bool SaveQuestions(List<FormQuestion> formQuestions)
        {
            return Db.UpsertRecords<FormQuestion>("FormQuestions", formQuestions);
        }

        public static int GenerateRandomFormId()
        {
            var randomId = 0;
            var rng = new Random();
            randomId = rng.Next(10000);

            return randomId;
        }

        public static bool SaveFormAnswers(IEnumerable<FormAnswer> answers)
        {
            return Db.UpsertRecords<FormAnswer>("FormAnswers", answers);
        }

        public static bool SaveForm(Form form)
        {
            return Db.UpsertRecord("Forms", form);
        }
        #endregion

        #region TOKEN
        public static bool SaveToken(EmployeeFormAnswerHistory token)
        {
            return Db.UpsertRecord("EmployeeFormAnswersHistory", token);
        }

        public static EmployeeFormAnswerHistory GetTokenForUnAnsweredEmployee(Employee employee)
        {
            if(employee != null)
            {
                var token = Db.ExecuteRecord<EmployeeFormAnswerHistory>("SELECT * FROM EmployeeFormAnswersHistory " +
                                                                        "WHERE EmployeeId = {0} AND DateAnswered IS NULL", employee.Id);

                return token != null ? token : null;
            }

            return null;
        }

        public static List<EmployeeFormAnswerHistory> GetAllEmployeeFormAnswers()
        {
            var tokens = Db.ExecuteRecords<EmployeeFormAnswerHistory>("SELECT * FROM EmployeeFormAnswersHistory").ToList();

            return tokens.Any() ? tokens : null;
        }

        public static bool SaveTokens(List<EmployeeFormAnswerHistory> tokens)
        {
            return Db.UpsertRecords<EmployeeFormAnswerHistory>("EmployeeFormAnswersHistory", tokens);
        }

        public static bool UpdateToken(string token)
        {
            var formToken = Db.ExecuteRecord<EmployeeFormAnswerHistory>("SELECT * FROM EmployeeFormAnswersHistory WHERE Token = {0}", token);

            formToken.DateAnswered = DateTime.Now;

            if(SaveToken(formToken))
            {
                return true;
            }

            return false;
        }
        #endregion
    }
}

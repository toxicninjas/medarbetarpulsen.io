﻿using Medarbetarpulsen.Core.DataModels;
using Medarbetarpulsen.Core.DataModels.FormModels;
using Medarbetarpulsen.Core.DataModels.ViewModels;
using Medarbetarpulsen.Core.Enums;
using Medarbetarpulsen.Core.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medarbetarpulsen.Core.BusinessLogic
{
    public static class FormHelper
    {
        public static FormViewModel ValidateToken(string token)
        {
            if (token.Contains(" "))
            {
                token = token.Replace(" ", "+");
            }

            var formToken = DbAccess.ValidateFormToken(token);

            if (formToken != null)
            {
                var form = DbAccess.GetCompleteForm(formToken.FormId);
                                                                      
                if(form != null)                                      
                {                                                     
                    return form;                                      
                }                                                     
            }                                                         
                                                                      
            return null;                                              
        }

        public static bool Create(FormViewModel form)
        {
            if(form != null)
            {
                var formIsCreated = DbAccess.CreateForm(form);
                var questionsIsCreated = CreateFormQuestions(form);

                if (formIsCreated && questionsIsCreated)
                {
                    return true;
                }
            }
            
            //We return false here if the form isn't created
            return false;
        }

        public static bool CreateFormQuestions(FormViewModel form)
        {
            var formQuestions = new List<FormQuestion>();

            foreach (var question in form.Questions)
            {
                var questionModel = new FormQuestion()
                {
                    FormId = form.Id,
                    Question = question.Question,
                    QuestionTitle = question.QuestionTitle,
                    FormQuestionTypeId = question.FormQuestionTypeId,
                    EditionId = form.EditionId
                };

                formQuestions.Add(questionModel);
            }

            return DbAccess.SaveQuestions(formQuestions) ? true : false;
        }

        //TODO: Need to break this up into smaller pieces
        public static WrapperViewModel Get(int formId, int editionId, int departmentId)
        {
            //Get All Answers
            var answers = DbAccess.GetFormAnswers(formId, editionId);

            //Get those who answered
            var answered = DbAccess.GetAnsweredEmployees(formId, editionId, departmentId);

            //Get those who didn't answer
            var notAnswered = DbAccess.GetUnansweredEmployees(formId, editionId, departmentId);

            //Get Form questions
            var questions = DbAccess.GetFormQuestions(formId, editionId);

            //Get employees for department
            var employees = DbAccess.GetEmployeesByDepartment(departmentId);

            var modelWrapper = new WrapperViewModel();

            foreach (var employee in employees)
            {
                //This is for stringAnswers/stringQuestions 
                var stringQuestions = GetFormStringQuestions(answers, questions, employee);

                var stringAnswers = GetFormStringAnswers(answers, questions, employee);
                

                foreach (var stringquestion in stringQuestions)
                {
                    //THIS IS TEXTANSWER
                    modelWrapper.TextAnswers.Add(new EmplTextAnswerViewModel()
                    {
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        EmailAddress = employee.EmailAddress,
                        Question = stringquestion.Question,
                        QuestionTitle = stringquestion.QuestionTitle,
                        TextAnswer = stringAnswers.FirstOrDefault(x => x.FormQuestionId == stringquestion.Id)?.StringAnswer
                    });
                }
            }

            if(modelWrapper.TextAnswers.Count != 0)
            {
                foreach (var answeredEmployee in answered)
                {
                    modelWrapper.AnswerRate.EmployeesAnswered.Add(answeredEmployee);
                }

                foreach (var unAnsweredEmployee in notAnswered)
                {
                    modelWrapper.AnswerRate.EmployeesNotAnswered.Add(unAnsweredEmployee);
                }

                var intQuestions = questions.Where(y => y.FormQuestionTypeId == (int)FormAnswerType.Int)?.ToList();

                foreach (var intQuestion in intQuestions)
                {
                    modelWrapper.IntAnswers.Add(new EmplIntAnswerViewModel()
                    {
                        Question = intQuestion.Question,
                        QuestionTitle = intQuestion.QuestionTitle,
                        //IntAnswer = intAnswers.FirstOrDefault(x => x.FormQuestionId == intQuestion.Id)?.IntAnswer,
                        Min = answers.Where(y => y.FormQuestionId == intQuestion.Id)?.Select(x => x.IntAnswer)?.Min() ?? 0,
                        Max = answers.Where(y => y.FormQuestionId == intQuestion.Id)?.Select(x => x.IntAnswer)?.Max() ?? 0,
                        Avg = answers.Where(y => y.FormQuestionId == intQuestion.Id)?.Select(x => x.IntAnswer)?.Average() ?? 0
                    });
                }
            }

            if (modelWrapper.IntAnswers.Count() != 0 &&
                modelWrapper.TextAnswers.Count() != 0 ||
                modelWrapper.AnswerRate.EmployeesAnswered.Count() != 0 ||
                modelWrapper.AnswerRate.EmployeesNotAnswered.Count() != 0)
            {
                return modelWrapper;
            }

            return null;
        }

        public static IEnumerable<FormQuestion> GetFormStringQuestions(List<FormAnswer> answers, List<FormQuestion> questions, Employee employee)
        {
            IEnumerable<FormQuestion> stringQuestions = from answer in answers
                                                        join question in questions
                                                             on answer.FormQuestionId equals question.Id
                                                        where answer.EmployeeId == employee.Id && question.FormQuestionTypeId == (int)FormAnswerType.String
                                                        select question;
            return stringQuestions;
        }

        public static IEnumerable<FormAnswer> GetFormStringAnswers(List<FormAnswer> answers, List<FormQuestion> questions, Employee employee)
        {
            IEnumerable<FormAnswer> stringAnswers = from answer in answers
                                                    join question in questions
                                                         on answer.FormQuestionId equals question.Id
                                                    where answer.EmployeeId == employee.Id && answer.FormQuestionTypeId == (int)FormAnswerType.String
                                                    select answer;
            return stringAnswers;
        }

        public static bool Submit(SubmitFormViewModel form)
        {
            //Retrieve FormAnswers and update database
            if (form.Answers.Count != 0 &&
               !string.IsNullOrEmpty(form.Token.Token) &&
               form.Token.DateAnswered == null)
            {
                foreach (var answer in form.Answers)
                {
                    answer.EmployeeId = form.Token.EmployeeId;
                    answer.FormId = form.Token.FormId;
                    answer.EditionId = form.Token.EditionId;
                    answer.DateCreated = DateTime.Now;
                }

                //Update DateAnswered in Database
                if (DbAccess.SaveFormAnswers(form.Answers) &&
                    DbAccess.UpdateToken(form.Token.Token))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool SendReminder(WrapperViewModel wrapper)
        {
            var tokenList = new List<EmployeeFormAnswerHistory>();

            //rename token here
            var tokens = DbAccess.GetAllEmployeeFormAnswers();

            //var buttcrack = wrapper.AnswerRate.EmployeesNotAnswered.Where(x => tokens.Any(y => y.EmployeeId == x.Id))?.ToList();

            tokenList = tokens.Where(x => wrapper.AnswerRate.EmployeesNotAnswered.Any(y => y.Id == x.EmployeeId))?.ToList();    
            
            if(tokenList.Any())
            {
                return EmailHelper.SendReminder("test123@mail.se", tokenList) ? true : false;
            }

            return false;
        }

        public static bool Send(SendFormViewModel viewModel)
        {
            if(viewModel != null)
            {
                var edition = DbAccess.GetEditionId(viewModel.FormId);

                if (edition != null)
                {
                    var tokenList = TokenHelper.Populate(viewModel, viewModel.Department.Employees, edition);

                    if (tokenList.Any() && DbAccess.SaveTokens(tokenList))
                    {
                        //TODO: [From] should be dynamic here (instead of hardcoded lala@lala.se)
                        if (EmailHelper.SendFormToEmployees("lala@lala.se", tokenList))
                        {
                            return true;
                        }
                    }
                }
            }
            
            return false;
        }
    }
}
